jupyterlab (4.0.11+ds1-2) unstable; urgency=medium

  * Improve error detection in d/rules.
  * Bug fix: "Use node-long package", thanks to Bastien Roucariès (Closes:
    #1067017).
  * Bug fix: "use pacckaged node-call-bind (provided package)", thanks to
    Bastien Roucariès (Closes: #1067019).
  * Bug fix: "please use node-get-intrinsic", thanks to Bastien Roucariès
    (Closes: #1067020).

 -- Roland Mas <lolando@debian.org>  Tue, 19 Mar 2024 16:49:20 +0100

jupyterlab (4.0.11+ds1-1) unstable; urgency=medium

  * New upstream release.
  * Bug fix: "CVE-2024-22420 CVE-2024-22421", thanks to Salvatore
    Bonaccorso (Closes: #1061221).

 -- Roland Mas <lolando@debian.org>  Mon, 22 Jan 2024 13:52:19 +0100

jupyterlab (4.0.10+ds1-1) unstable; urgency=medium

  * New upstream release.

 -- Roland Mas <lolando@debian.org>  Fri, 12 Jan 2024 16:01:12 +0100

jupyterlab (4.0.9+ds1-2) UNRELEASED; urgency=medium

  * Run jupyter-labextension build at package build time.

 -- Roland Mas <lolando@debian.org>  Wed, 03 Jan 2024 17:06:14 +0100

jupyterlab (4.0.9+ds1-1) unstable; urgency=medium

  * New upstream release.

 -- Roland Mas <lolando@debian.org>  Tue, 26 Dec 2023 14:50:00 +0100

jupyterlab (4.0.8+ds1-3) UNRELEASED; urgency=medium

  * Add missing Build-Depends.
  * Pre-build Jupyterlab assets at package build time.
  * Bug fix: "Jupyter lab does not start up", thanks to Christian Holm
    Christensen (Closes: #1060390).

 -- Roland Mas <lolando@debian.org>  Tue, 26 Dec 2023 14:45:22 +0100

jupyterlab (4.0.8+ds1-2) unstable; urgency=medium

  * Update debian/copyright slightly.

 -- Roland Mas <lolando@debian.org>  Tue, 14 Nov 2023 11:44:52 +0100

jupyterlab (4.0.8+ds1-1) unstable; urgency=medium

  * New upstream release.
  * Exclude yarn.js.

 -- Roland Mas <lolando@debian.org>  Sat, 11 Nov 2023 14:10:59 +0100

jupyterlab (3.6.3-4) unstable; urgency=medium

  * Add missing runtime dependency on node-jupyterlab.

 -- Roland Mas <lolando@debian.org>  Fri, 21 Jul 2023 18:27:15 +0200

jupyterlab (3.6.3-3) unstable; urgency=medium

  * Clean up build-dependencies since the Javascript parts are no longer
    built from this source package.

 -- Roland Mas <lolando@debian.org>  Fri, 21 Jul 2023 18:14:12 +0200

jupyterlab (3.6.3-2) unstable; urgency=medium

  * Node modules are now packaged separately in node-jupyterlab.
  * Initial release.

 -- Roland Mas <lolando@debian.org>  Thu, 18 May 2023 17:04:44 +0200

jupyterlab (3.6.3-1) UNRELEASED; urgency=medium

  * Import new upstream release.

 -- Roland Mas <lolando@debian.org>  Mon, 01 May 2023 16:48:01 +0200

jupyterlab (3.6.1-1) UNRELEASED; urgency=medium

  * Import new upstream release.

 -- Roland Mas <lolando@debian.org>  Wed, 08 Mar 2023 17:29:24 +0100

jupyterlab (3.5.0~rc0-1) UNRELEASED; urgency=medium

  * Import new upstream pre-release.

 -- Roland Mas <lolando@debian.org>  Wed, 19 Oct 2022 15:55:29 +0200

jupyterlab (3.4.8-1) UNRELEASED; urgency=medium

  * Import new upstream release.

 -- Roland Mas <lolando@debian.org>  Wed, 05 Oct 2022 10:09:23 +0200

jupyterlab (3.2.4-1) UNRELEASED; urgency=medium

  [ Julien Puydt ]
  * Initial release. (Closes: #934258)

  [ Roland Mas ]
  * Continuing Julien's work.

 -- Roland Mas <lolando@debian.org>  Wed, 09 Feb 2022 20:06:59 +0100
